Config                        = {}
Config.DrawDistance           = 100
Config.MarkerSize             = {x = 1.5, y = 1.5, z = 1.0}
Config.MarkerColor            = {r = 255, g = 255, b = 0}
Config.EnterExitMarkerColor   = {r = 102, g = 102, b = 204}
Config.RoomMenuMarkerColor    = {r = 102, g = 204, b = 102}
Config.MarkerType             = 1
Config.Zones                  = {}
Config.Warehouses             = {}
Config.EnablePlayerManagement = false -- If set to true you need esx_realestateagentjob
Config.Locale                 = 'en'

Config.WarehouseSetup = {
  Warehouse_Small = {
    interiorID = 235777,
    zones = {
      exit = {x=1087.44,y=-3099.40,z=-40.0},
      inside = {x=1089.02,y=-3099.28,z=-40.0},
      roomMenu = {x=1088.38,y=-3101.30,z=-40.0}
    },
    --price = 250000
    price = 0
  },
  Warehouse_Medium = {
    interiorID = 235521,
    zones = {
      exit = {x=1048.12,y=-3097.20,z=-40.0},
      inside = {x=1050.37,y=-3097.81,z=-40.0},
      roomMenu = {x=1049.05,y=-3100.62,z=-40.0}
    },
    --price = 500000
    price = 0
  },
  Warehouse_Large = {
    interiorID = 236033,
    zones = {
      exit = {x=992.37,y=-3097.77,z=-40.0},
      inside = {x=993.91,y=-3097.94,z=-40.0},
      roomMenu = {x=994.5,y=-3099.96,z=-40.0}
    },
    --price = 750000
    price = 0
  }
}
