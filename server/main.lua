ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function GetWarehouse(name)
  for i=1, #Config.Warehouses, 1 do
    if Config.Warehouses[i].name == name then
      return Config.Warehouses[i]
    end
  end
end

function SetWarehouseOwned(name, label, price, owner)
  MySQL.Async.execute(
    'INSERT INTO owned_warehouses (name, label, owner) VALUES (@name, @label, @owner)',
    {
      ['@name']   = name,
      ['@label']  = label,
      ['@owner']  = owner
    }, function(rowsChanged)
		local xPlayer = ESX.GetPlayerFromIdentifier(owner)

		if xPlayer then
			TriggerClientEvent('esx_warehouse:setWarehouseOwned', xPlayer.source, name, true)
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('purchased_for', price))
		end
	end)

  MySQL.Async.execute(
    'UPDATE warehouses SET is_owned = @is_owned WHERE name = @name',
    {
      ['@is_owned'] = 1,
      ['@name']    = name
    }
  )
end

function RemoveOwnedWarehouse(name, owner)
  MySQL.Async.execute(
    'DELETE FROM owned_warehouses WHERE name = @name',
    {
      ['@name']  = name
    },
    function()
      local xPlayers = ESX.GetPlayers()
      local xPlayer = ESX.GetPlayerFromId(xPlayers[i])

      for i=1, #xPlayers, 1 do
        local xPlayer = ESX.GetPlayerFromId(xPlayers[i])

        if xPlayer.identifier == owner then
          TriggerClientEvent('esx_warehouse:setWarehouseOwned', xPlayer.source, name, false)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('sold_warehouse'))
          break
        end
      end
    end
  )

  MySQL.Async.execute(
    'UPDATE warehouses SET is_owned = @is_owned WHERE name = @name',
    {
      ['@is_owned'] = 0,
      ['@name']    = name
    }
  )
end



AddEventHandler('onMySQLReady', function ()
  MySQL.Async.fetchAll('SELECT * FROM warehouses', {}, function(warehouses)

    for i=1, #warehouses, 1 do
      local entering  = nil
      local outside   = nil
      local isOwned  = nil

      if warehouses[i].entering ~= nil then
        entering = json.decode(warehouses[i].entering)
      end

      if warehouses[i].outside ~= nil then
        outside = json.decode(warehouses[i].outside)
      end

      if warehouses[i].is_owned == 0 then
        isOwned = false
      else
        isOwned = true
      end

      table.insert(Config.Warehouses, {
        name      = warehouses[i].name,
        entering  = entering,
        outside   = outside,
        type      = warehouses[i].type,
        isOwned   = isOwned
      })
    end
  end)
end)


AddEventHandler('esx_ownedwarehouse:getOwnedWarehouses', function(cb)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_warehouses',
    {},
    function(result)
      local warehouses = {}

      for i=1, #result, 1 do
        table.insert(warehouses, {
          id     = result[i].id,
          name   = result[i].name,
          owner  = result[i].owner,
          props = result[i].props
        })
      end
      cb(warehouses)
    end
  )
end)


AddEventHandler('esx_warehouse:setWarehouseOwned', function(name, price, owner)
  SetWarehouseOwned(name, price, owner)
end)


AddEventHandler('esx_warehouse:removeOwnedWarehouse', function(name, owner)
  RemoveOwnedWarehouse(name, owner)
end)



RegisterServerEvent('esx_warehouse:buyWarehouse')
AddEventHandler('esx_warehouse:buyWarehouse', function(warehouseName, label, price)
  local xPlayer  = ESX.GetPlayerFromId(source)
  local warehouse = GetWarehouse(warehouseName)

  if price <= xPlayer.get('money') then
    xPlayer.removeMoney(price)
    SetWarehouseOwned(warehouseName, label, price, xPlayer.identifier)
  else
    TriggerClientEvent('esx:showNotification', source, _U('not_enough'))
  end
end)


RegisterServerEvent('esx_warehouse:removeOwnedWarehouse')
AddEventHandler('esx_warehouse:removeOwnedWarehouse', function(warehouseName, price)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.addMoney(price)
  RemoveOwnedWarehouse(warehouseName, xPlayer.identifier)
end)


AddEventHandler('esx_warehouse:removeOwnedWarehouseIdentifier', function(warehouseName, identifier)
  RemoveOwnedWarehouse(warehouseName, identifier)
end)


RegisterServerEvent('esx_warehouse:saveLastWarehouse')
AddEventHandler('esx_warehouse:saveLastWarehouse', function(warehouse)
  local xPlayer = ESX.GetPlayerFromId(source)

  MySQL.Async.execute(
    'UPDATE users SET last_warehouse = @last_warehouse WHERE identifier = @identifier',
    {
      ['@last_warehouse'] = warehouse,
      ['@identifier']    = xPlayer.identifier
    }
  )
end)


RegisterServerEvent('esx_warehouse:deleteLastWarehouse')
AddEventHandler('esx_warehouse:deleteLastWarehouse', function()
  local xPlayer = ESX.GetPlayerFromId(source)

  MySQL.Async.execute(
    'UPDATE users SET last_warehouse = NULL WHERE identifier = @identifier',
    {
      ['@identifier']    = xPlayer.identifier
    }
  )
end)


RegisterServerEvent('esx_warehouse:getItem')
AddEventHandler('esx_warehouse:getItem', function(owner, type, item, count)
	local _source      = source
	local xPlayer      = ESX.GetPlayerFromId(_source)
	local xPlayerOwner = ESX.GetPlayerFromIdentifier(owner)

	if type == 'item_standard' then
		local sourceItem = xPlayer.getInventoryItem(item)

		TriggerEvent('esx_addoninventory:getInventory', 'warehouse', xPlayerOwner.identifier, function(inventory)
			local inventoryItem = inventory.getItem(item)

			-- is there enough in the warehouse?
			if count > 0 and inventoryItem.count >= count then

				-- can the player carry the said amount of x item?
				if sourceItem.limit ~= -1 and (sourceItem.count + count) > sourceItem.limit then
					TriggerClientEvent('esx:showNotification', _source, _U('player_cannot_hold'))
				else
					inventory.removeItem(item, count)
					xPlayer.addInventoryItem(item, count)
					TriggerClientEvent('esx:showNotification', _source, _U('have_withdrawn', count, inventoryItem.label))
				end
			else
				TriggerClientEvent('esx:showNotification', _source, _U('not_enough_in_warehouse'))
			end
		end)

	elseif type == 'item_account' then
		TriggerEvent('esx_addonaccount:getAccount', 'warehouse' .. item, xPlayerOwner.identifier, function(account)
			local roomAccountMoney = account.money

			if roomAccountMoney >= count then
				account.removeMoney(count)
				xPlayer.addAccountMoney(item, count)
			else
				TriggerClientEvent('esx:showNotification', _source, _U('amount_invalid'))
			end
		end)

	elseif type == 'item_weapon' then
		TriggerEvent('esx_datastore:getDataStore', 'warehouse', xPlayerOwner.identifier, function(store)
			local storeWeapons = store.get('weapons') or {}
			local weaponName   = nil
			local ammo         = nil

			for i=1, #storeWeapons, 1 do
				if storeWeapons[i].name == item then
					weaponName = storeWeapons[i].name
					ammo       = storeWeapons[i].ammo

					table.remove(storeWeapons, i)
					break
				end
			end

			store.set('weapons', storeWeapons)
			xPlayer.addWeapon(weaponName, ammo)
		end)
	end
end)


RegisterServerEvent('esx_warehouse:putItem')
AddEventHandler('esx_warehouse:putItem', function(owner, type, item, count)
	local _source      = source
	local xPlayer      = ESX.GetPlayerFromId(_source)
	local xPlayerOwner = ESX.GetPlayerFromIdentifier(owner)

	if type == 'item_standard' then
		local playerItemCount = xPlayer.getInventoryItem(item).count

		if playerItemCount >= count and count > 0 then
			TriggerEvent('esx_addoninventory:getInventory', 'warehouse', xPlayerOwner.identifier, function(inventory)
				xPlayer.removeInventoryItem(item, count)
				inventory.addItem(item, count)
				TriggerClientEvent('esx:showNotification', _source, _U('have_deposited', count, inventory.getItem(item).label))
			end)
		else
			TriggerClientEvent('esx:showNotification', _source, _U('invalid_quantity'))
		end

	elseif type == 'item_account' then
		local playerAccountMoney = xPlayer.getAccount(item).money

		if playerAccountMoney >= count and count > 0 then
			xPlayer.removeAccountMoney(item, count)

			TriggerEvent('esx_addonaccount:getAccount', 'warehouse_' .. item, xPlayerOwner.identifier, function(account)
				account.addMoney(count)
			end)
		else
			TriggerClientEvent('esx:showNotification', _source, _U('amount_invalid'))
		end

	elseif type == 'item_weapon' then
		TriggerEvent('esx_datastore:getDataStore', 'warehouse', xPlayerOwner.identifier, function(store)
			local storeWeapons = store.get('weapons') or {}

			table.insert(storeWeapons, {
				name = item,
				ammo = count
			})

			store.set('weapons', storeWeapons)
			xPlayer.removeWeapon(item)
		end)
	end
end)


--[[RegisterServerEvent('esx_warehouse:enableProp')
AddEventHandler('esx_warehouse:enableProp', function(prop, hostThisInstance, warehouse)
  TriggerClientEvent('esx_warehouse:enableProp', -1, prop, hostThisInstance)
  local props_table = {}
  print("\nSELECT FROM owned_warehouses WHERE name = " .. warehouse.name)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_warehouses WHERE name = @name',
    {
      ['@name']  = warehouse.name
    },
    function(ownedWarehouses)
      props_table = json.decode(ownedWarehouses[1].props)
      print("\nprops_table before: " .. json.encode(props_table))
      table.insert(props_table, prop)
      print("attempt to insert: " .. prop)
      print("\nprops_table after: " .. json.encode(props_table))
      TriggerEvent('esx_warehouse:updateProps', warehouse, props_table)
    end
  )
end)


RegisterServerEvent('esx_warehouse:disableProp')
AddEventHandler('esx_warehouse:disableProp', function(prop, hostThisInstance, warehouse)
  TriggerClientEvent('esx_warehouse:disableProp', -1, prop, hostThisInstance)
  local props_table = {}
  print("\nSELECT FROM owned_warehouses WHERE name = " .. warehouse.name)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_warehouses WHERE name = @name',
    {
      ['@name']  = warehouse.name
    },
    function(ownedWarehouses)
      props_table = json.decode(ownedWarehouses[1].props)
      print("\nprops_table before: " .. json.encode(props_table))
      for k,v in pairs(props_table) do
        --print("k: " .. k .. " v: " .. v)
        if v == prop then
          table.remove(props_table,k)
          print("attempt to remove: " .. v)
          print("\nprops_table after: " .. json.encode(props_table))
          TriggerEvent('esx_warehouse:updateProps', warehouse, props_table)
        end
      end
    end
  )
end)]]


--[[RegisterServerEvent('esx_warehouse:installEquipment')
AddEventHandler('esx_warehouse:installEquipment', function(item, host)
  local xPlayer = ESX.GetPlayerFromId(source)
  local item = xPlayer.getInventoryItem(item)
  xPlayer.removeInventoryItem(item.name, 1) --might need to be propName
  TriggerClientEvent('esx:showNotification', source, _U('installed_equipment', item.label))
end)]]


--[[RegisterServerEvent('esx_warehouse:updateProps') -- ONLY EXECUTE THIS WHEN THE PLAYER IS INSIDE THE INSTANCE
AddEventHandler('esx_warehouse:updateProps', function(warehouse, props_table)
  print("\nUPDATE owned_warehouses SET props = " .. json.encode(props_table) .. " WHERE name = " .. warehouse.name)
  MySQL.Async.execute(
    'UPDATE owned_warehouses SET props = @props WHERE name = @name',
    {
      ['@props'] = json.encode(props_table), --using our new table
      ['@name'] = warehouse.name
    }
  )
end)


ESX.RegisterServerCallback('esx_warehouse:getProps', function(source, cb, name, hostThisInstance)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_warehouses WHERE name = @name',
    {
      ['@name']  = name
    },
    function(ownedWarehouses)
      cb(json.decode(ownedWarehouses[1].props), hostThisInstance)
      --print("hostThisInstance: " .. hostThisInstance)
    end
  )
end)]]


ESX.RegisterServerCallback('esx_warehouse:getWarehouseType', function(source, cb, name)
  MySQL.Async.fetchAll(
    'SELECT * FROM warehouses WHERE name = @name',
    {
      ['@name'] = name
    },
    function(warehouses)
      cb(warehouses[1].type)
    end
  )
end)



ESX.RegisterServerCallback('esx_warehouse:getWarehouseLabel', function(source, cb, name)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_warehouses WHERE name = @name',
    {
      ['@name'] = name
    },
    function(ownedWarehouses)
      cb(ownedWarehouses[1].label)
    end
  )
end)


ESX.RegisterServerCallback('esx_warehouse:getWarehouseIsOwned', function(source, cb, name)
  MySQL.Async.fetchAll(
    'SELECT * FROM warehouses WHERE name = @name',
    {
      ['@name'] = name
    },
    function(warehouses)
      cb(warehouses[1].is_owned) -- errors here
    end
  )
end)


ESX.RegisterServerCallback('esx_warehouse:getWarehouses', function(source, cb)
  cb(Config.Warehouses)
end)

function table.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

ESX.RegisterServerCallback('esx_warehouse:getOwnedWarehouses', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll('SELECT * FROM owned_warehouses WHERE owner = @owner', {
	['@owner'] = xPlayer.identifier
  }, function(ownedWarehouses)
		local warehouses = {}

		for i=1, #ownedWarehouses, 1 do
			table.insert(warehouses, ownedWarehouses[i].name)
		end

		cb(warehouses)
	end)
end)


ESX.RegisterServerCallback('esx_warehouse:getLastWarehouse', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)

  MySQL.Async.fetchAll(
    'SELECT * FROM users WHERE identifier = @identifier',
    {
      ['@identifier'] = xPlayer.identifier
    },
    function(users)
      cb(users[1].last_warehouse)
    end
  )
end)


ESX.RegisterServerCallback('esx_warehouse:getWarehouseInventory', function(source, cb, owner)
	local xPlayer    = ESX.GetPlayerFromIdentifier(owner)
	local blackMoney = 0
	local items      = {}
	local weapons    = {}

	TriggerEvent('esx_addonaccount:getAccount', 'warehouse_black_money', xPlayer.identifier, function(account)
		blackMoney = account.money
	end)

	TriggerEvent('esx_addoninventory:getInventory', 'warehouse', xPlayer.identifier, function(inventory)
		items = inventory.items
	end)

	TriggerEvent('esx_datastore:getDataStore', 'warehouse', xPlayer.identifier, function(store)
		local storeWeapons = store.get('weapons') or {}
	end)

	cb({
		blackMoney = blackMoney,
		items      = items,
		weapons    = weapons
	})
end)


ESX.RegisterServerCallback('esx_warehouse:getPlayerInventory', function(source, cb)
	local xPlayer    = ESX.GetPlayerFromId(source)
	local blackMoney = xPlayer.getAccount('black_money').money
	local items      = xPlayer.inventory

	cb({
		blackMoney = blackMoney,
		items      = items
	})
end)


--[[
ESX.RegisterServerCallback('esx_warehouse:getPlayerDressing', function(source, cb)
	local xPlayer  = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'warehouse', xPlayer.identifier, function(store)
		local count    = store.count('dressing')
		local labels   = {}

		for i=1, count, 1 do
			local entry = store.get('dressing', i)
			table.insert(labels, entry.label)
		end

		cb(labels)
	end)
end)

ESX.RegisterServerCallback('esx_warehouse:getPlayerOutfit', function(source, cb, num)
	local xPlayer  = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'warehouse', xPlayer.identifier, function(store)
		local outfit = store.get('dressing', num)
		cb(outfit.skin)
	end)
end)

RegisterServerEvent('esx_warehouse:removeOutfit')
AddEventHandler('esx_warehouse:removeOutfit', function(label)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'warehouse', xPlayer.identifier, function(store)
		local dressing = store.get('dressing') or {}

		table.remove(dressing, label)
		store.set('dressing', dressing)
	end)
end)
]]
